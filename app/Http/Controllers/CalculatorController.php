<?php

namespace App\Http\Controllers;

use App\Http\Services\IssPositionService;
use App\Http\Services\PoznanDistanceCalculatorService;
use Illuminate\Http\JsonResponse;

class CalculatorController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/calculate",
     *     @OA\Response(
     *     response="200", description="Poznan to ISS distance in meters",
     *     @OA\JsonContent(
     *          @OA\Property(
     *          property="data",
     *          type="object",
     *              @OA\Property(
     *                  property="distanceFromPoznan",
     *                  type="string",
     *                  example="123.31 m",
     * )
     * )
     * )
     * )
     * )
     */
    public function calculate(
        IssPositionService $issPositionService,
        PoznanDistanceCalculatorService $poznanDistanceCalculatorService
    ): JsonResponse
    {
        $issPositonData = $issPositionService->getIssPositonData();
        $distanceInMeters =
            $poznanDistanceCalculatorService->getDistanceInMeters($issPositonData);
        return response()->json(['data' => ['distanceFromPoznan' => $distanceInMeters . 'm']]);
    }
}
