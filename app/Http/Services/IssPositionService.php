<?php


namespace App\Http\Services;


use Illuminate\Support\Facades\Http;

class IssPositionService
{
    public function getIssPositonData(): ?object
    {
        $issData = $this->getIssPosition();

        if ($issData->message === 'success') {
            return $issData->iss_position;
        } else {
            return null;
        }
    }

    private function getIssPosition(): object
    {
        try {
            return json_decode(Http::get('http://api.open-notify.org/iss-now'));
        } catch (\Throwable $exception) {
            report($exception);
        }
    }
}
