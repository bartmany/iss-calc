<?php


namespace App\Http\Services;


class PoznanDistanceCalculatorService
{
    const longitude = 16.9211;
    const latitude = 52.4144;

    public function getDistanceInMeters(object $issPositionData): float
    {
        $earthRadius = 6371000;
        $diffLat = $issPositionData->latitude - self::latitude;
        $diffLng = $issPositionData->longitude - self::longitude;
        $a = sin($diffLat / 2)
            * sin($diffLat / 2)
            + cos(self::latitude)
            * cos($issPositionData->latitude)
            * sin($diffLng / 2)
            * sin($diffLng / 2);

        $b = 2 * asin(sqrt($a));

        return round(($earthRadius * $b), 2);
    }
}
