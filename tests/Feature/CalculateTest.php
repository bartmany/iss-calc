<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class CalculateTest extends TestCase
{

    public function testRouteExists()
    {
        $response = $this->get('/api/v1/calculate');

        $response->assertStatus(JsonResponse::HTTP_OK);
    }

    public function testCorrectResponse()
    {
        $response = $this->get('/api/v1/calculate');
        $response->assertStatus(JsonResponse::HTTP_OK);
        $response->assertJsonStructure(['data']);
    }
}
